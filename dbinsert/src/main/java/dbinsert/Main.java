package dbinsert;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Main implements Runnable {

	public static Connection con;

	private static ConcurrentHashMap<String, ConcurrentLinkedQueue<Struct>> queues;
	private static List<Struct> testingList = new ArrayList<Struct>();
	
	
	public static void main(String[] args) throws IOException {
		queues = new ConcurrentHashMap<String, ConcurrentLinkedQueue<Struct>>();
		try {
			con = DriverManager.getConnection("jdbc:mysql://51.158.69.222/Paurus?autoReconnect=true", "paurus",
					"gmpfRqqpq6309kQ9");
		} catch (SQLException e) {
			e.printStackTrace();
			return;
		}

		(new Thread(new Main())).start();
		
		BufferedReader br = new BufferedReader(new FileReader("fo_random.txt"));//replace with steady stream...
		String line = br.readLine();//skip first line
		while((line = br.readLine())!=null) {
			String[] spl = line.split("\\|(?=(?:[^\']*\'[^\']*\')*[^\']*$)");//regex explained on https://stackoverflow.com/questions/18893390/splitting-on-comma-outside-quotes
			Struct tmp = new Struct(spl[0], Integer.parseInt(spl[1]), spl[2], spl.length==4?spl[3]:"");
			if(!queues.containsKey(spl[0])){
				queues.put(spl[0], new ConcurrentLinkedQueue<Struct>());
			}
			queues.get(spl[0]).add(tmp);
			testingList.add(tmp);
		}
		br.close();
		//for test purposes only
		//simulates processing data.
		while(true) {
			for (Struct struct : testingList) {
				struct.processed |= Math.random()*10<1;
			}
		}

	}

	public void run() {
		while (true) {// to keep running despite errors
			try (PreparedStatement ps = con.prepareStatement(
					"INSERT INTO `task2` (`MATCH_ID`, `MARKET_ID`, `OUTCOME_ID`, `SPECIFIERS`, `date_insert`) VALUES (?, ?, ?, ?, CURRENT_TIMESTAMP);");) {
				for (Iterator<Entry<String, ConcurrentLinkedQueue<Struct>>> iterator = queues.entrySet().iterator(); iterator.hasNext();) {
					ConcurrentLinkedQueue<Struct> queue = iterator.next().getValue();
					Struct next = queue.peek();// get but not remove
					while (next != null && next.isDone()) {//iterate all processed data in the queue
						queue.poll();// remove head
						ps.clearParameters();
						ps.setString(1, next.getMatchId());
						ps.setInt(2, next.getMarketId());
						ps.setString(3, next.getOutcomeId());
						ps.setString(4, next.getSpecifiers());
						ps.addBatch();//add head row to batch
						next = queue.peek();
						System.out.println(next);//testing only
					}
					if(queue.isEmpty()) {
						iterator.remove();
					}
				}
				ps.executeBatch();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

	}

	/**
	 * Struct represents one row to be inserted after being processed.
	 */
	public static class Struct {
		boolean processed = false;
		String matchId;
		int marketId;
		String outcomeId;
		String specifiers;
		Date d;

		public Struct(String matchId, int marketId, String outcomeId, String specifiers) {
			super();
			this.matchId = matchId;
			this.marketId = marketId;
			this.outcomeId = outcomeId;
			this.specifiers = specifiers;
			this.d = new Date();
		}

		public String getMatchId() {
			return matchId;
		}

		public void setMatchId(String matchId) {
			this.matchId = matchId;
		}

		public int getMarketId() {
			return marketId;
		}

		public void setMarketId(int marketId) {
			this.marketId = marketId;
		}

		public String getOutcomeId() {
			return outcomeId;
		}

		public void setOutcomeId(String outcomeId) {
			this.outcomeId = outcomeId;
		}

		public String getSpecifiers() {
			return specifiers;
		}

		public void setSpecifiers(String specifiers) {
			this.specifiers = specifiers;
		}
		
		
		
		@Override
		public String toString() {
			return "Struct [processed=" + processed + ", matchId=" + matchId + ", dateReeceived="+d.getTime()+"]";
		}

		/**
		 * Returns true if object was properly proccessed, false otherwise
		 * 
		 * @return
		 */
		public boolean isDone() {
			return processed;
		}
	}
}
